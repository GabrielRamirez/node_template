import express from 'express';
import dotenv from 'dotenv';
import api from './src/routes/api.mjs';

dotenv.config();

const app = express();

const PORT = process.env.PORT || 3000;

app.use('/api', api);

app.listen(PORT, () => {
    console.log(`http://localhost:${PORT}`);
})